package mcb.techlead.accountapi;

import java.time.LocalDateTime;

public class Transaction {
    public Integer txn_id;
    public String txn_number;
    public String debit_account;
    public String credit_account;
    public Double amount;
    public String currency;
    public LocalDateTime timestamp;

    public Transaction(Integer txn_id, String txn_number, String debit_account, String credit_account, Double amount, String currency, LocalDateTime timestamp) {
        this.txn_id = txn_id;
        this.txn_number = txn_number;
        this.debit_account = debit_account;
        this.credit_account = credit_account;
        this.amount = amount;
        this.currency = currency;
        this.timestamp = timestamp;
    }
}