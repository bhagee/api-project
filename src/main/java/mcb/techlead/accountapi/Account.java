package mcb.techlead.accountapi;


public class Account {
    public Integer account_id;
    public String account_number;
    public String account_type;
    public String first_name;
    public String last_name;
    public Double account_balance;
    public String account_currency;
    public Boolean is_active;

    public Account(Integer account_id, String account_number, String account_type, String first_name, String last_name, Double account_balance, String account_currency, Boolean is_active) {
        this.account_id = account_id;
        this.account_number = account_number;
        this.account_type = account_type;
        this.first_name = first_name;
        this.last_name = last_name;
        this.account_balance = account_balance;
        this.account_currency = account_currency;
        this.is_active = is_active;
    }
}
