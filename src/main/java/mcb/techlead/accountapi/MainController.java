package mcb.techlead.accountapi;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
public class MainController {

    @Operation(hidden = true)
    @GetMapping(value = "/", produces = "application/json")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @PostMapping (value = "/account/create", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> create_account(@RequestBody Account account) {

        if (account.first_name.length() <= 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"First Name must be greater than 1\", \"status\":\"failed\"}");
        } else if (account.last_name.length() <= 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Last Name must be greater than 1\", \"status\":\"failed\"}");
        } else if (account.account_balance < 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Balance must be at least 1\", \"status\":\"failed\"}");
        } else if (!(account.account_type.equals("Current") || account.account_type.equals("Savings"))) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Type Must Be Current Or Savings\", \"status\":\"failed\"}");
        } else if (!account.account_currency.equals("MUR")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Currency Must Be MUR\", \"status\":\"failed\"}");
        } else if (account.is_active == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"No is_active parameter provided\", \"status\":\"failed\"}");
        } else {

            ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;

            Integer account_id;
            String account_number;

            if (AccountsData.size() == 0) {
                account_id = 1;
                account_number = "000000000001";
            } else {
                AccountapiApplication.Last_Account_Id += 1;
                account_id = AccountapiApplication.Last_Account_Id;
                account_number = AccountapiApplication.Last_Account_Id.toString();
                while (account_number.length() < 12) {
                    account_number = "0" + account_number;
                }
            }

            Account myacc = new Account(account_id,account_number,account.account_type,account.first_name,account.last_name,account.account_balance,account.account_currency,account.is_active);

            AccountsData.add(myacc);
            return ResponseEntity.status(HttpStatus.OK).body(myacc);

        }
    }

    @Operation(hidden = true)
    @GetMapping(value = "/list", produces = "application/json")
    public ResponseEntity<Object> list() {

        ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;

        if (AccountsData.size() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"No Account In Database\", \"status\":\"failed\"}");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(AccountsData);
        }

    }

    @GetMapping(value = "/account/{target_account_number}", produces = "application/json")
    public ResponseEntity<Object> account_query(@PathVariable String target_account_number) {

        ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;

        Optional<Account> myacc = AccountsData.stream().filter(a -> a.account_number.equals(target_account_number)).findFirst();

        if (target_account_number.length() < 12) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (myacc.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Not Found\", \"status\":\"failed\"}");
        } else if (!myacc.get().is_active) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Is Not Active\", \"status\":\"failed\"}");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(myacc.get());
        }

    }

    @GetMapping(value = "/account/{target_account_number}/activate", produces = "application/json")
    public ResponseEntity<Object> account_activate(@PathVariable String target_account_number) {

        ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;

        Optional<Account> myacc = AccountsData.stream().filter(a -> a.account_number.equals(target_account_number)).findFirst();

        if (target_account_number.length() < 12) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (myacc.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Not Found\", \"status\":\"failed\"}");
        } else if (myacc.get().is_active) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Is Already Active\", \"status\":\"failed\"}");
        } else {
            myacc.get().is_active = true;
            return ResponseEntity.status(HttpStatus.OK).body(("{\"msg\":\"Account Has Been Activated\", \"status\":\"success\"}"));
        }

    }

    @GetMapping(value = "/account/{target_account_number}/deactivate", produces = "application/json")
    public ResponseEntity<Object> account_deactivate(@PathVariable String target_account_number) {

        ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;

        Optional<Account> myacc = AccountsData.stream().filter(a -> a.account_number.equals(target_account_number)).findFirst();

        if (target_account_number.length() < 12) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (myacc.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Not Found\", \"status\":\"failed\"}");
        } else if (!myacc.get().is_active) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Is Already Deactivated\", \"status\":\"failed\"}");
        } else {
            myacc.get().is_active = false;
            return ResponseEntity.status(HttpStatus.OK).body("{\"msg\":\"Account Has Been Deactivated\", \"status\":\"success\"}");
        }

    }

    @GetMapping(value = "/account/{target_account_number}/transactions", produces = "application/json")
    public ResponseEntity<Object> txn_query(@PathVariable String target_account_number) {

        ArrayList<Transaction> TransactionsData = AccountapiApplication.TransactionsData;

        List<Transaction> txns = TransactionsData.stream().filter(x -> x.debit_account.equals(target_account_number) || x.credit_account.equals(target_account_number)).collect(Collectors.toList());

        if (target_account_number.length() < 12) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Account Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (txns.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"No Transactions Found For Particular Account\", \"status\":\"failed\"}");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(txns);
        }
    }

    @PostMapping(value = "/account-transfer", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> account_transfer(@RequestBody Transaction transaction) {


        ArrayList<Account> AccountsData = AccountapiApplication.AccountsData;
        ArrayList<Transaction> TransactionsData = AccountapiApplication.TransactionsData;

        Optional<Account> debit_account = AccountsData.stream().filter(a -> a.account_number.equals(transaction.debit_account)).findFirst();
        Optional<Account> credit_account = AccountsData.stream().filter(a -> a.account_number.equals(transaction.credit_account)).findFirst();

        if(transaction.debit_account == transaction.credit_account){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Debit Account And Credit Account Cannot Be Same\", \"status\":\"failed\"}");
        } else if(transaction.debit_account.length() < 12){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Debit Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (debit_account.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Debit Account Not Found\", \"status\":\"failed\"}");
        } else if (!debit_account.get().is_active) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Debit Account is Not Active\", \"status\":\"failed\"}");
        } else if (transaction.credit_account.length() < 12) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Credit Number Must Be Greater Or Equal To 12\", \"status\":\"failed\"}");
        } else if (credit_account.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Credit Account Not Found\", \"status\":\"failed\"}");
        } else if (!credit_account.get().is_active) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Credit Account is Not Active\", \"status\":\"failed\"}");
        } else if (transaction.amount <=  1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"All Transactions Must Be Greater Than 1\", \"status\":\"failed\"}");
        } else if (debit_account.get().account_balance <= transaction.amount) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"error\":\"Debit Account Has Insufficient Funds\", \"status\":\"failed\"}");
        } else {

            Account new_debit = debit_account.get();
            new_debit.account_balance = debit_account.get().account_balance - transaction.amount;
            AccountsData.remove(debit_account.get());
            AccountsData.add(new_debit);

            Account new_credit = credit_account.get();
            new_credit.account_balance = credit_account.get().account_balance + transaction.amount;
            AccountsData.remove(credit_account.get());
            AccountsData.add(new_credit);

            Integer txn_id;
            String txn_number;

            if(TransactionsData.size() == 0) {
                txn_id = 1;
            } else {
                txn_id = TransactionsData.get(TransactionsData.size() - 1).txn_id + 1;
            }

            txn_number = "FT" + txn_id.toString() + AppLogic.generateAlphaNumeric().toUpperCase();


            Transaction mytxn = new Transaction(txn_id, txn_number, transaction.debit_account, transaction.credit_account, transaction.amount,transaction.currency, LocalDateTime.now());
            TransactionsData.add(mytxn);

            return ResponseEntity.status(HttpStatus.OK).body(mytxn);

        }
    }

}