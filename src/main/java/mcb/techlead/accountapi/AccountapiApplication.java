package mcb.techlead.accountapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class AccountapiApplication {

	static Integer Last_Account_Id = 1;
	static ArrayList<Account> AccountsData = new ArrayList<>();
	static ArrayList<Transaction> TransactionsData = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(AccountapiApplication.class, args);
	}

}